import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators, NgForm, FormGroupDirective } from '@angular/forms';

import { Stock } from '../../shared/models/stock';
import { NotificationService } from '../../shared/services/notification/notification.service';
import { ENDPOINT } from '../../shared/constants/endpoint';
import { StockService } from '../../shared/services/stock/stock.service';

@Component({
  selector: 'app-stock',
  templateUrl: './stock.component.html',
  styleUrls: ['./stock.component.css']
})

export class StockComponent implements OnInit {

  stocks: any;
  detailedStock: any;
  stockForm: FormGroup;

  @ViewChild(FormGroupDirective, { static: false }) stockFormDirective: FormGroupDirective;

  constructor(
    protected stockService: StockService,
    protected notificationService: NotificationService,
    protected formBuilder: FormBuilder
  ) { }

  ngOnInit() {
    this.getAllStocks();

    this.stockForm = this.formBuilder.group({
      ticker: ['', Validators.required],
      company: ['', Validators.required],
      market: ['', Validators.required]
    });
  }

  getStockDetails(stock: Stock): void {
    this.stockService.getSpecifiedStock(ENDPOINT + 'stocks/' + stock.id).subscribe(
      stockDetailsData => this.detailedStock = stockDetailsData,
      err => this.notificationService.displayNotification("Unable to get market details!")
    );
  }

  addStock(): void {
    if (this.stockForm.invalid) return;

    const stockToAdd: Stock = {
      companyName: this.stockForm.get('company').value,
      market: {
        name: this.stockForm.get('market').value
      },
      ticker: this.stockForm.get('ticker').value
    }

    this.stockService.addStock(ENDPOINT + 'stocks/', stockToAdd).subscribe(
      postResponse => {
        this.notificationService.displayNotification("Added!");
        setTimeout(() => this.stockFormDirective.resetForm(), 0);
        this.getAllStocks();
      },
      err => this.notificationService.displayNotification("Unable to add this stock!")
    );
  }

  deleteStock(): void {
    this.stockService.deleteStock(ENDPOINT + 'stocks/' + this.detailedStock.id).subscribe(
      deleteResponse => {
        this.notificationService.displayNotification("Deleted!");
        this.detailedStock = null;
        this.getAllStocks();
      },
      err => this.notificationService.displayNotification("Unable to delete this stock!")
    );
  }

  private getAllStocks() {
    this.stockService.getAllStocks(ENDPOINT + 'stocks/').subscribe(
      allStocksData => this.stocks = allStocksData,
      err => this.notificationService.displayNotification("Unable to get all stocks!")
    );
  }

}
