import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';

import { MainComponent } from './main.component';
import { routes } from '../../shared/constants/routes';
import { StockComponent } from '../stock/stock.component';
import { MarketComponent } from '../market/market.component';
import { ExternalsModule } from '../../shared/modules/externals-module';

fdescribe('MainComponent', () => {
  let component: MainComponent;
  let fixture: ComponentFixture<MainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        MainComponent,
        StockComponent,
        MarketComponent
      ],
      imports: [
        RouterModule,
        RouterModule.forRoot(routes),
        FormsModule,
        ExternalsModule
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create the MainComponent', () => {
    expect(component).toBeTruthy();
  });

});
