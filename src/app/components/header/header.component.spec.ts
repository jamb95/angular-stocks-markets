import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { RouterModule, Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule } from '@angular/forms';
import { Location } from "@angular/common";

import { HeaderComponent } from './header.component';
import { routes } from '../../shared/constants/routes';
import { StockComponent } from '../stock/stock.component';
import { MarketComponent } from '../market/market.component';
import { ExternalsModule } from '../../shared/modules/externals-module';

fdescribe('HeaderComponent', () => {
  let component: HeaderComponent;
  let fixture: ComponentFixture<HeaderComponent>;
  let location: Location;
  let router: Router;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        HeaderComponent,
        StockComponent,
        MarketComponent
      ],
      imports: [
        RouterModule,
        RouterTestingModule.withRoutes(routes),
        FormsModule,
        ExternalsModule
      ]
    })
      .compileComponents();

    router = TestBed.get(Router);
    location = TestBed.get(Location);

    fixture = TestBed.createComponent(HeaderComponent);
    router.initialNavigation();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create the HeaderComponent', () => {
    expect(component).toBeTruthy();
  });

  it('navigate to "" takes you to /stocks', fakeAsync(() => {
    router.navigate(['']);
    tick();
    expect(location.path()).toBe('/stocks');
  }));

  it('navigate to "stocks" takes you to /stocks', fakeAsync(() => {
    router.navigate(['stocks']);
    tick();
    expect(location.path()).toBe('/stocks');
  }));

  it('navigate to "markets" takes you to /markets', fakeAsync(() => {
    router.navigate(['markets']);
    tick();
    expect(location.path()).toBe('/markets');
  }));

});
