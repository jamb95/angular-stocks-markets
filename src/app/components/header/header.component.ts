import { Component, OnInit } from '@angular/core';

import { routes } from '../../shared/constants/routes';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})

export class HeaderComponent implements OnInit {

  appRoutes: any = routes;

  constructor() {
    this.appRoutes[0].label = 'Stocks';
    this.appRoutes[1].label = 'Markets';
  }

  ngOnInit() {
  }

}
