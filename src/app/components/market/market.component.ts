import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormGroupDirective } from '@angular/forms';

import { Market } from '../../shared/models/market';
import { NotificationService } from '../../shared/services/notification/notification.service';
import { ENDPOINT } from '../../shared/constants/endpoint';
import { MarketService } from '../../shared/services/market/market.service';

@Component({
  selector: 'app-markets',
  templateUrl: './market.component.html',
  styleUrls: ['./market.component.css']
})

export class MarketComponent implements OnInit {

  markets: any;
  detailedMarket: any;
  marketForm: FormGroup;

  @ViewChild(FormGroupDirective, { static: false }) marketFormDirective: FormGroupDirective;

  constructor(
    protected marketService: MarketService,
    protected notificationService: NotificationService,
    protected formBuilder: FormBuilder
  ) { }

  ngOnInit() {
    this.getAllMarkets();

    this.marketForm = this.formBuilder.group({
      name: ['', Validators.required]
    });
  }

  getMarketDetails(market: Market): void {
    this.marketService.getSpecifiedMarket(ENDPOINT + 'markets/' + market.id).subscribe(
      marketDetailsData => this.detailedMarket = marketDetailsData,
      err => this.notificationService.displayNotification("Unable to get market details!")
    );
  }

  addMarket(): void {
    if (this.marketForm.invalid) return;

    const marketToAdd: Market = {
      name: this.marketForm.get('name').value
    }

    this.marketService.addMarket(ENDPOINT + 'markets/', marketToAdd).subscribe(
      postResponse => {
        this.notificationService.displayNotification("Added!");
        setTimeout(() => this.marketFormDirective.resetForm(), 0);
        this.getAllMarkets();
      },
      err => this.notificationService.displayNotification("Unable to add this market!")
    );
  }

  deleteMarket(): void {
    this.marketService.deleteMarket(ENDPOINT + 'markets/' + this.detailedMarket.id).subscribe(
      deleteResponse => {
        this.notificationService.displayNotification("Deleted!");
        this.detailedMarket = null;
        this.getAllMarkets();
      },
      err => this.notificationService.displayNotification("Market is in use!")
    );
  }

  private getAllMarkets() {
    this.marketService.getAllMarkets(ENDPOINT + 'markets/').subscribe(
      allMarketsData => this.markets = allMarketsData,
      err => this.notificationService.displayNotification("Unable to get all markets!")
    );
  }

}
