import { async, ComponentFixture, TestBed, tick, fakeAsync } from '@angular/core/testing';

import { MarketComponent } from './market.component';
import { FormsModule } from '@angular/forms';
import { ExternalsModule } from 'src/app/shared/modules/externals-module';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

fdescribe('MarketComponent', () => {
  let component: MarketComponent;
  let fixture: ComponentFixture<MarketComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MarketComponent],
      imports: [
        FormsModule,
        ExternalsModule,
        HttpClientModule,
        BrowserAnimationsModule
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MarketComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create MarketComponent', () => {
    expect(component).toBeTruthy();
  });

  // it('should get all markets from the database', fakeAsync(() => {
  //   component.ngOnInit;
  //   tick();
  //   let availableMarkets = component.markets;
  //   expect(availableMarkets).toBe;
  // }));

});
