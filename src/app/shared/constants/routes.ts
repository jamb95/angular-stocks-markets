import { Routes } from '@angular/router';

import { StockComponent } from '../../components/stock/stock.component';
import { MarketComponent } from '../../components/market/market.component';

export const routes: Routes = [
    { path: 'stocks', component: StockComponent },
    { path: 'markets', component: MarketComponent },
    { path: '', redirectTo: 'stocks', pathMatch: 'full' }
];