import { Injectable } from '@angular/core';

import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root'
})

export class NotificationService {

  constructor(
    protected _snackBar: MatSnackBar
  ) { }

  displayNotification(content: string, buttonLabel: string = "Close"): void {
    this._snackBar.open(content, buttonLabel, {
      duration: 3000
    });
  }

}
