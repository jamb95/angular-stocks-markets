import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

import { Stock } from '../../models/stock';

@Injectable({
  providedIn: 'root'
})

export class StockService {

  constructor(
    protected httpClient: HttpClient
  ) { }

  getAllStocks(url: string): Observable<Stock[]> {
    return this.httpClient.get<Stock[]>(url);
  }

  getSpecifiedStock(url: string): Observable<Stock> {
    return this.httpClient.get<Stock>(url);
  }

  addStock(url: string, market: Stock): Observable<Stock> {
    return this.httpClient.post<Stock>(url, market);
  }

  deleteStock(url: string): Observable<Stock> {
    return this.httpClient.delete<Stock>(url);
  }

}
