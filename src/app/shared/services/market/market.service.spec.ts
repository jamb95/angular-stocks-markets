import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { MarketService } from './market.service';
import { Market } from '../../models/market';
import { ENDPOINT } from '../../constants/endpoint';
import { Observable } from 'rxjs';

fdescribe('MarketService', () => {
  let injector: TestBed;
  let service: MarketService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [MarketService],
    });

    injector = getTestBed();
    service = injector.get(MarketService);
    httpMock = injector.get(HttpTestingController);
  });

  it('should create MarketService', () => {
    const service: MarketService = TestBed.get(MarketService);
    expect(service).toBeTruthy();
  });

  afterEach(() => {
    httpMock.verify();
  });

  const dummyGetAllMarketsResponse: Market[] = [{ "id": 6, "name": "NY" }];
  const dummyGetSpecifiedMarketResponse: Market = { "id": 6, "name": "NY" };
  const dummyAddMarketObject: Market = { "id": 100, "name": "London" };

  it('getAllMarkets() should return an array of Market objects', () => {
    const testCaseUrl = ENDPOINT + 'markets/';

    service.getAllMarkets(testCaseUrl).subscribe((res) => {
      expect(res).toEqual(dummyGetAllMarketsResponse);
    });

    const req = httpMock.expectOne(testCaseUrl);
    expect(req.request.method).toBe('GET');
    req.flush(dummyGetAllMarketsResponse);
  });

  it('getSpecifiedMarket() should return a single Market object', () => {
    const testCaseUrl = ENDPOINT + 'markets/6';

    service.getSpecifiedMarket(testCaseUrl).subscribe((res) => {
      expect(res).toEqual(dummyGetSpecifiedMarketResponse);
    });

    const req = httpMock.expectOne(testCaseUrl);
    expect(req.request.method).toBe('GET');
    req.flush(dummyGetSpecifiedMarketResponse);
  });

  it('addMarket() should POST and return data', () => {
    const testCaseUrl = ENDPOINT + 'markets/';
    const req = httpMock.expectOne(testCaseUrl);
    expect(req.request.method).toBe('POST');
  });

});
