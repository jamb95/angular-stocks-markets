import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { Market } from '../../models/market';

@Injectable({
  providedIn: 'root'
})

export class MarketService {

  constructor(
    protected httpClient: HttpClient
  ) { }

  getAllMarkets(url: string): Observable<Market[]> {
    return this.httpClient.get<Market[]>(url);
  }

  getSpecifiedMarket(url: string): Observable<Market> {
    return this.httpClient.get<Market>(url);
  }

  addMarket(url: string, market: Market): Observable<Market> {
    return this.httpClient.post<Market>(url, market);
  }

  deleteMarket(url: string): Observable<Market> {
    return this.httpClient.delete<Market>(url);
  }

}
