export interface Stock {
    id?: number
    companyName: string,
    market: {
        id?: number,
        name: string
    },
    ticker: string
}